package com.designpatterns.behavioral.visitorpattern;

public class HomeLoanApplication implements Printable
{
	private String name;
	private String city;
	private int amount;

	public HomeLoanApplication(String name, String city, int amount) {
		super();
		this.name = name;
		this.city = city;
		this.amount = amount;
	}

	@Override
	public void printUsing(Printer printer) 
	{
		printer.print(this);
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public int getAmount() {
		return amount;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "HomeLoanApplication [name=" + name + ", city=" + city
				+ ", amount=" + amount + "]";
	}

}
