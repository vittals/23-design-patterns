package com.designpatterns.behavioral.visitorpattern;

public interface Printer 
{
	void print(CarLoanApplication carLoanApplication);
	void print(HomeLoanApplication homeLoanApplication);

}
