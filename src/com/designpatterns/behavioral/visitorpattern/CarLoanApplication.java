package com.designpatterns.behavioral.visitorpattern;

public class CarLoanApplication implements Printable 
{
	private String applicantName;
	private String address;
	private int amount;

	public CarLoanApplication(String applicantName, String address, int amount) {
		super();
		this.applicantName = applicantName;
		this.address = address;
		this.amount = amount;
	}

	@Override
	public void printUsing(Printer printer) {
		printer.print(this);
	}

	public String getApplicantName() {
		return applicantName;
	}

	public String getAddress() {
		return address;
	}

	public int getAmount() {
		return amount;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "CarLoanApplication [applicantName=" + applicantName
				+ ", address=" + address + ", amount=" + amount + "]";
	}


}
