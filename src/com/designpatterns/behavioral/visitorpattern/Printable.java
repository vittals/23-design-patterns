package com.designpatterns.behavioral.visitorpattern;

public interface Printable 
{
	void printUsing(Printer printer);
}
