package com.designpatterns.behavioral.visitorpattern;

public class SimplePrinter implements Printer
{

	@Override
	public void print(CarLoanApplication carLoanApplication) {
		System.out.println("Simple Printer:\n This is a car loan application of :" + carLoanApplication.getApplicantName());
		
	}

	@Override
	public void print(HomeLoanApplication homeLoanApplication) {
		System.out.println("Simple Printer:\n This is a home loan application of :" + homeLoanApplication.getName());
		
	}

}
