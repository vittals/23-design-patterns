package com.designpatterns.behavioral.visitorpattern;

import java.util.ArrayList;
import java.util.List;


public class VisitorPatternDemo 
{
	public static void main(String[] args) 
	{
		List<Printable> printables = new ArrayList<Printable>();
		printables.add(new CarLoanApplication("John Smith", "Home Blvd, My City", 20000));
		printables.add(new HomeLoanApplication("Cathy Shields", "Boston", 1000000));
		for (Printable printable : printables)
		{
			printable.printUsing(new SimplePrinter());
			printable.printUsing(new AdvancedPrinter());
			System.out.println("-----------------\n");
		}
		
	}

}
