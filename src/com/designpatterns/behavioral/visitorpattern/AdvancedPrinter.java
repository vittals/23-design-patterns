package com.designpatterns.behavioral.visitorpattern;

public class AdvancedPrinter implements Printer
{

	@Override
	public void print(CarLoanApplication carLoanApplication) {
		System.out.println("Advanced printer: \n" + carLoanApplication.toString());
	}

	@Override
	public void print(HomeLoanApplication homeLoanApplication) {
		System.out.println("Advanced printer: \n" + homeLoanApplication.toString());
	}
}
