package com.designpatterns.behavioral.iteratorpattern;

public class IteratorPatternDemo 
{
	
	public static void main(String[] args) 
	{
		Loan [] loans = new Loan[5];
		loans [0] = new Loan("Sam",130);
		loans [1] = new Loan("Peter", 90);
		loans [2] = new Loan ("Carey", 150);
		loans [3] = new Loan("Mitch", 102);
		loans [4] = new Loan("Daniel", 200);
		OutstandingAmountIterator iterator = new OutstandingAmountIterator(loans, 100);
		while (iterator.hasNext())
		{
			Loan loan = iterator.next();
			System.out.println(loan.getName() + " has balance = " + loan.getBalance());
		}
	}
}
