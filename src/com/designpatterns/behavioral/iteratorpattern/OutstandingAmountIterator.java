package com.designpatterns.behavioral.iteratorpattern;

public class OutstandingAmountIterator implements LoanIterator<Loan>
{
	private int position = 0;
	private long balanceToComapre;
	private Loan [] loans;

	public OutstandingAmountIterator(Loan[] loans, int balanceToCompare) {
		super();
		this.loans = loans;
		this.balanceToComapre = balanceToCompare;
	}

	@Override
	public void reset() {
		position = 0;
		
	}

	@Override
	public Loan next() {
		int thisPosition = position;
		position++;
		if (loans[thisPosition].getBalance() > balanceToComapre)
		{
			return loans[thisPosition];
		}
		else
		{
			return next();
		}
	
	}

	@Override
	public Loan current() {
		// TODO Auto-generated method stub
		return loans[position];
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return position < loans.length;
	}

}
