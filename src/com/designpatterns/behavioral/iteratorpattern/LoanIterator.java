package com.designpatterns.behavioral.iteratorpattern;

public interface LoanIterator<E> 
{
	void reset();
	
	E next();
	
	E current();
	
	boolean hasNext();

}
