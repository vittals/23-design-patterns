package com.designpatterns.behavioral.iteratorpattern;

public class Loan 
{
	private String name;
	private int balance;
	public String getName() {
		return name;
	}
	public long getBalance() {
		return balance;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	public Loan()
	{
		
	}
	
	public Loan(String name, int balance) {
		super();
		this.name = name;
		this.balance = balance;
	}
	

}
