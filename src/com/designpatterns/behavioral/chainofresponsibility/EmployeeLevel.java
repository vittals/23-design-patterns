package com.designpatterns.behavioral.chainofresponsibility;

public enum EmployeeLevel 
{
	CLERK, OFFICER, ASSISTANT_MANAGER, MANAGER;

}
