package com.designpatterns.behavioral.chainofresponsibility;

import java.util.List;

public class LoanSanctionResponsibility 
{
	List <Employee> employees;

	public LoanSanctionResponsibility(List<Employee> employees) {
		super();
		this.employees = employees;
	}
	
	public void sanctionLoan(LoanLevel loanLevel)
	{
		for (Employee anEmployee : employees)
		{
			if (anEmployee.canSanction(loanLevel.getLevel()))
			{
				System.out.println(loanLevel.name() + " loan sanctioned by " + anEmployee.getLevel().name());
				System.out.println("------------------");
				return;
			}
			else
			{
				System.out.println(anEmployee.getLevel().name() + " cannot sanction " + loanLevel.name() + " loan or is absent, checking next level!");
			}
			
		}
	}
	
	

}
