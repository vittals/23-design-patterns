package com.designpatterns.behavioral.chainofresponsibility;

public enum LoanLevel {
	NONE(0),
	PERSONAL(1),
	CAR(2),
	HOME(3);
	
	private final int level;

	private LoanLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}
}
