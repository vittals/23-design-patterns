package com.designpatterns.behavioral.chainofresponsibility;

public class Employee 
{
	private EmployeeLevel level;
	private LoanLevel canSanction;
	private boolean isPresent;
	
	
	
	public Employee(EmployeeLevel level, LoanLevel canSanction, boolean isPresent) {
		super();
		this.level = level;
		this.canSanction = canSanction;
		this.isPresent = isPresent;
	}
	
	public boolean canSanction(int num)
	{
		return isPresent && num <= canSanction.getLevel();
	}
	
	public EmployeeLevel getLevel() {
		return level;
	}
	public LoanLevel getCanSanction() {
		return canSanction;
	}
	public void setLevel(EmployeeLevel level) {
		this.level = level;
	}
	public void setCanSanction(LoanLevel canSanction) {
		this.canSanction = canSanction;
	}

	public boolean isPresent() {
		return isPresent;
	}

	public void setPresent(boolean isPresent) {
		this.isPresent = isPresent;
	}
	

}
