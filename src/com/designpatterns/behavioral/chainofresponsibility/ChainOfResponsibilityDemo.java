package com.designpatterns.behavioral.chainofresponsibility;

import java.util.ArrayList;
import java.util.List;

public class ChainOfResponsibilityDemo 
{
	
	public static List<Employee> buildChain()
	{
		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(EmployeeLevel.CLERK, LoanLevel.NONE, true));
		employees.add(new Employee(EmployeeLevel.OFFICER, LoanLevel.PERSONAL, true));
		employees.add(new Employee(EmployeeLevel.ASSISTANT_MANAGER, LoanLevel.CAR, true));
		employees.add(new Employee(EmployeeLevel.MANAGER, LoanLevel.HOME, true));
		return employees;
	}
	
	public static List<Employee> buildAnotherChain()
	{
		List<Employee> employees = new ArrayList<>();
		employees.add(new Employee(EmployeeLevel.CLERK, LoanLevel.NONE, true));
		employees.add(new Employee(EmployeeLevel.OFFICER, LoanLevel.PERSONAL, false));
		employees.add(new Employee(EmployeeLevel.ASSISTANT_MANAGER, LoanLevel.CAR, false));
		employees.add(new Employee(EmployeeLevel.MANAGER, LoanLevel.HOME, true));
		return employees;
	}	
	
	public static void main(String[] args) 
	{
		buildChain();
		LoanLevel personalLoanApplication = LoanLevel.PERSONAL;
		LoanLevel homeLoanApplication = LoanLevel.HOME;
		System.out.println("***********************************\n Trying to sanction Personal and Home loan ...");
		LoanSanctionResponsibility responsibility = new LoanSanctionResponsibility(buildChain());
		responsibility.sanctionLoan(personalLoanApplication);
		responsibility.sanctionLoan(homeLoanApplication);
		responsibility = new LoanSanctionResponsibility(buildAnotherChain());
		System.out.println("***********************************\n Trying to sanction Personal and Home loan ...(with Officer and Assistant Manager absent)");
		responsibility.sanctionLoan(personalLoanApplication);
		responsibility.sanctionLoan(homeLoanApplication);
		
	}

}
