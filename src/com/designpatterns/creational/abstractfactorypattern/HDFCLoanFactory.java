package com.designpatterns.creational.abstractfactorypattern;

public class HDFCLoanFactory extends AbstractLoanFactory
{
   public HDFCLoanFactory()
   {
	   setBank(Bank.HDFC);
   }
}
