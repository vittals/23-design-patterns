package com.designpatterns.creational.abstractfactorypattern;

public class HomeLoan extends Loan
{


	public HomeLoan(Bank bank) {
		super(bank, LoanType.HOME);
	}

	@Override
	public float getInterest() 
	{
		if (getBank().equals(Bank.HDFC))
		{
			return 8.75f;
		}
		else if (getBank().equals(Bank.ICICI))
		{
			return 8.85f;
		}
		else
		{
			return 9f;
		}
	}

}
