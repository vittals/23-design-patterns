package com.designpatterns.creational.abstractfactorypattern;

public abstract class AbstractLoanFactory 
{
	protected Bank bank;
	
	protected Loan getLoan(LoanType loanType)
	{
		if (loanType.equals(LoanType.CAR))
		{
			return new CarLoan(bank);
		}
		else if (loanType.equals(LoanType.HOME))
		{
			return new HomeLoan(bank);
		}
		else
		{
			return null;
		}	
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

}
