package com.designpatterns.creational.abstractfactorypattern;

public abstract class Loan 
{
	private Bank bank;
	private LoanType loanType;
	public Loan(Bank bank, LoanType loanType)
	{
		this.bank = bank;
		this.loanType = loanType;
	}
	
	public abstract float getInterest();

	public Bank getBank() {
		return bank;
	}

	public LoanType getLoanType() {
		return loanType;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public void setLoanType(LoanType loanType) {
		this.loanType = loanType;
	}

	@Override
	public String toString() {
		return "Loan [bank=" + bank + ", loanType=" + loanType + "]";
	}

}
