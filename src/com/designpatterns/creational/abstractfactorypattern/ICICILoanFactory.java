package com.designpatterns.creational.abstractfactorypattern;

public class ICICILoanFactory extends AbstractLoanFactory
{
   public ICICILoanFactory()
   {
	   setBank(Bank.ICICI);
   }
}

