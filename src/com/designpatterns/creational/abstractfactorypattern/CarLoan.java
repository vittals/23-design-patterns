package com.designpatterns.creational.abstractfactorypattern;

public class CarLoan extends Loan
{

	public CarLoan(Bank bank) {
		super(bank, LoanType.CAR);
	}

	@Override
	public float getInterest() 
	{
		if (getBank().equals(Bank.HDFC))
		{
			return 12.75f;
		}
		else if (getBank().equals(Bank.ICICI))
		{
			return 12.85f;
		}
		else
		{
			return 20f;
		}
	}

}
