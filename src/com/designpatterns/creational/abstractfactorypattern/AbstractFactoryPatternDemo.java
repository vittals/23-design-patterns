package com.designpatterns.creational.abstractfactorypattern;

public class AbstractFactoryPatternDemo 
{

	public static void main(String[] args) 
	{
		AbstractLoanFactory hdfcFactory = new HDFCLoanFactory();
		AbstractLoanFactory iciciFactory = new ICICILoanFactory();
		System.out.println(hdfcFactory.getLoan(LoanType.HOME));
		System.out.println(iciciFactory.getLoan(LoanType.HOME));
		System.out.println(hdfcFactory.getLoan(LoanType.CAR));
		System.out.println(iciciFactory.getLoan(LoanType.CAR));
	}
}
