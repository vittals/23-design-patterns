package com.designpatterns.creational.builderpattern;

public class LoanApplicationBuilder 
{
	private String firstName;
	private String lastName;
	private int age;
	private int income;
	private String gender;
	private String nationality;
	
	public LoanApplicationBuilder(String firstName, String lastName)
	{
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public LoanApplicationBuilder withAge(int age)
	{
		this.age = age;
		return this;
	}
	
	public LoanApplicationBuilder withIncome(int income)
	{
		this.income = income;
		return this;
	}
	
	public LoanApplicationBuilder withGender(String gender)
	{
		this.gender = gender;
		return this;
	}

	public LoanApplicationBuilder withNationality(String nationality)
	{
		this.nationality = nationality;
		return this;
	}
	
	public LoanApplication build()
	{
		LoanApplication loanApplication = new LoanApplication(this);
		return loanApplication;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getAge() {
		return age;
	}

	public int getIncome() {
		return income;
	}

	public String getGender() {
		return gender;
	}

	public String getNationality() {
		return nationality;
	}


}
