package com.designpatterns.creational.builderpattern;

public class LoanApplication 
{
	private String firstName;
	private String lastName;
	private int age;
	private int income;
	private String gender;
	private String nationality;
	
	public LoanApplication(LoanApplicationBuilder builder)
	{
		this.firstName = builder.getFirstName();
		this.lastName = builder.getLastName();
		this.gender = builder.getGender();
		this.income = builder.getIncome();
		this.age = builder.getAge();
		this.nationality = builder.getNationality();
	}
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getAge() {
		return age;
	}
	public int getIncome() {
		return income;
	}
	public String getGender() {
		return gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setIncome(int income) {
		this.income = income;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	@Override
	public String toString() {
		return "LoanApplication received with [firstName=" + firstName + ", lastName="
				+ lastName + ", age=" + age + ", income=" + income
				+ ", gender=" + gender + ", nationality=" + nationality + "]";
	}
	
	

}
