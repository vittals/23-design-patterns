package com.designpatterns.creational.builderpattern;

public class BuilderPatternDemo 
{
	public static void main(String[] args) 
	{
		LoanApplicationBuilder builder = new LoanApplicationBuilder("John", "Smith");
		LoanApplication loanApplication = 
				builder.withAge(35).withNationality("American").build();
		System.out.println(loanApplication.toString());
		
	}

}
