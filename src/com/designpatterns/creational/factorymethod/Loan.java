package com.designpatterns.creational.factorymethod;

public abstract class Loan implements Cloneable
{
	private LoanType type = null;

	abstract float getInterest();

	public LoanType getType() {
		return type;
	}

	public void setType(LoanType type) {
		this.type = type;
	}

	public Loan clone() throws
	CloneNotSupportedException 
	{ 
		return (Loan) super.clone(); 
	} 
}
