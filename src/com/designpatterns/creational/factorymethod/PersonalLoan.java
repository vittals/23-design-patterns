package com.designpatterns.creational.factorymethod;

public class PersonalLoan extends Loan
{

	PersonalLoan() {
		setType(LoanType.PERSONAL);
	}
	@Override
	float getInterest() {
		return 16.5f;
	}
	
	

}
