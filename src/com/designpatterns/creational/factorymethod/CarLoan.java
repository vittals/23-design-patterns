package com.designpatterns.creational.factorymethod;


public class CarLoan extends Loan
{
	CarLoan()
	{
		setType(LoanType.CAR);
	}

	@Override
	float getInterest() {
		return 11.5f;
	}

}
