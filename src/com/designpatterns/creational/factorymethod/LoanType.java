package com.designpatterns.creational.factorymethod;

public enum LoanType {
	HOME, CAR, PERSONAL

}
