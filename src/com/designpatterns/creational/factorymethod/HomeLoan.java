package com.designpatterns.creational.factorymethod;

public class HomeLoan extends Loan
{
	HomeLoan()
	{
		setType(LoanType.HOME);
	}

	@Override
	float getInterest() {
		return 8.5f;
	}

}
