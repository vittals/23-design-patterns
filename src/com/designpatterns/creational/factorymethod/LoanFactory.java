package com.designpatterns.creational.factorymethod;

import java.util.Hashtable;

public class LoanFactory 
{
	Hashtable<LoanType, Loan> tableOfLoans = new Hashtable<>();
	public LoanFactory() {
		tableOfLoans.put(LoanType.HOME, new HomeLoan());
		tableOfLoans.put(LoanType.CAR, new CarLoan());
		tableOfLoans.put(LoanType.PERSONAL, new PersonalLoan());
	}
	
	public Loan getLoan(LoanType loanType)
	{
		try {
			return tableOfLoans.get(loanType).clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
