package com.designpatterns.creational.factorymethod;

public class FactoryPatternDemo 
{
	public static void main(String[] args) 
	{
		LoanFactory factory = new LoanFactory();
		Loan loan = null;
		for (LoanType aLoanType : LoanType.values())
		{
			loan = factory.getLoan(aLoanType);
			System.out.println(loan.getType().name() + " loan interest rate is = " + loan.getInterest() + "%");			
		}

 
	}

}
