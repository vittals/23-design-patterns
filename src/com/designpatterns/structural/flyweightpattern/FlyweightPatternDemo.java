package com.designpatterns.structural.flyweightpattern;

public class FlyweightPatternDemo 
{
	public static void main(String[] args) {
		String [] loanTypes = { "Car", "Home", "Car", "Personal", "Home"};
		for (String aType : loanTypes)
		{
			LoanApprover approver = ApproverFactory.getApprover(aType);
			approver.approve();
		}
	}
}
