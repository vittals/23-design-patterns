package com.designpatterns.structural.flyweightpattern;

import java.util.HashMap;

public class ApproverFactory 
{
	private static HashMap<String, LoanApprover> approvers = new HashMap<>();
	
	public static LoanApprover getApprover(String type)
	{
		LoanApprover approver = approvers.get(type);
		if (approver == null)
		{
			System.out.println("Creating new approver for " + type + " loan");
			approver = new LoanApprover(type);
			approvers.put(type, approver);
		}
		else
		{
			System.out.println("Reusing existing approver for " + type + " loan");
		}
		return approver;
	}

}
