package com.designpatterns.structural.flyweightpattern;

public class LoanApprover 
{
	private String type;
	
	public LoanApprover(String type) {
		super();
		this.type = type;
	}

	public void approve()
	{
		System.out.println("Approved " + type + " Loan");
	}
}
