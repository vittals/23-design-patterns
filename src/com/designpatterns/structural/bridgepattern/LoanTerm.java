package com.designpatterns.structural.bridgepattern;

public interface LoanTerm 
{
	float getFactor(Loan loan);
}
