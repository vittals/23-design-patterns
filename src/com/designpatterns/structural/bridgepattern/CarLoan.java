package com.designpatterns.structural.bridgepattern;

public class CarLoan implements Loan
{
	LoanTerm loanTerm;

	public CarLoan(LoanTerm loanTerm) {
		super();
		this.loanTerm = loanTerm;
	}

	@Override
	public float getInterest() {
		return 11.5f * loanTerm.getFactor(this);
	}

}
