package com.designpatterns.structural.bridgepattern;

public class ShortTermLoan implements LoanTerm
{

	@Override
	public float getFactor(Loan loan) {
		return loan instanceof CarLoan ? 1.2f : 1f;
	}

}
