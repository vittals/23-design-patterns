package com.designpatterns.structural.bridgepattern;

public class HomeLoan implements Loan
{
	LoanTerm loanTerm;
	
	public HomeLoan(LoanTerm loanTerm) {
		super();
		this.loanTerm = loanTerm;
	}

	@Override
	public float getInterest() {
		return 8.5f * loanTerm.getFactor(this);
	}

}
