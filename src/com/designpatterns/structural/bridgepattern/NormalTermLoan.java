package com.designpatterns.structural.bridgepattern;

public class NormalTermLoan implements LoanTerm 
{

	@Override
	public float getFactor(Loan loan) {
		return 1;
	}

}
