package com.designpatterns.structural.bridgepattern;

public interface Loan 
{
	float getInterest();
}
