package com.designpatterns.structural.bridgepattern;

public class BridgePatternDemo 
{
	public static void main(String[] args) 
	{
		CarLoan normalCarLoan = new CarLoan(new NormalTermLoan());
		CarLoan shortTermCarLoan = new CarLoan(new ShortTermLoan());
		HomeLoan normalHomeLoan = new HomeLoan(new NormalTermLoan());
		HomeLoan shortTermHomeLoan = new HomeLoan(new ShortTermLoan());
		
		System.out.println(" Car Loan interest rate for normal term = " + normalCarLoan.getInterest() + "%" );
		System.out.println(" Car Loan interest rate for short term = " + shortTermCarLoan.getInterest() + "%" );
		System.out.println(" Home Loan interest rate for normal term = " + normalHomeLoan.getInterest() + "%" );
		System.out.println(" Home Loan interest rate for short term = " + shortTermHomeLoan.getInterest() + "%" );
		
	}

}
