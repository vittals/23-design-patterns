package com.designpatterns.structural.facadepattern;

public class FacadePatternDemo 
{
	public static void main(String[] args) 
	{
		LoanApproverFacade facade = new LoanApproverFacade();
		facade.approveHomeLoan();
		System.out.println(" -------------------- ");
		facade.approveCarLoan();
	}
}
