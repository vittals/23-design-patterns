package com.designpatterns.structural.facadepattern;

public interface LoanApprover 
{
	void approve();

}
