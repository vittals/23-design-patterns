package com.designpatterns.structural.facadepattern;

public class HomeLoanApprover implements LoanApprover
{

	@Override
	public void approve() 
	{
		System.out.println("Checking home loan application");
		System.out.println("Checking applicant's credit score");
		System.out.println("Checking property documents");
		System.out.println("Approved home loan!");
	}

}
