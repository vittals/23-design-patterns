package com.designpatterns.structural.facadepattern;

public class LoanApproverFacade 
{
	HomeLoanApprover homeLoanApprover = new HomeLoanApprover();
	CarLoanApprover carLoanApprover = new CarLoanApprover();
	
	public void approveHomeLoan()
	{
		homeLoanApprover.approve();
	}
	
	public void approveCarLoan()
	{
		carLoanApprover.approve();
	}
}