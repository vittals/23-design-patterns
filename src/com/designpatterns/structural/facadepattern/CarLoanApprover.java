package com.designpatterns.structural.facadepattern;

public class CarLoanApprover implements LoanApprover
{

	@Override
	public void approve() 
	{
		System.out.println("Checking car loan application");
		System.out.println("Checking applicant's credit score");
		System.out.println("Checking car history");
		System.out.println("Approved car loan!");		
		
	}

}
