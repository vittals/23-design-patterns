package com.designpatterns.structural.adaptorpattern;

public class BostonBankLoan 
{
	private BostonBankApplication application;
	public void apply(BostonBankApplication application)
	{
		this.application = application;
		System.out.println(application.getFullName() + " applied for loan at Boston Bank");
	}
	
	public BostonBankApplication getApplication() {
		return application;
	}
	public void setApplication(BostonBankApplication application) {
		this.application = application;
	}

}
