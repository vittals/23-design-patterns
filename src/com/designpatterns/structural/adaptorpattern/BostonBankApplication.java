package com.designpatterns.structural.adaptorpattern;

public class BostonBankApplication 
{
	private String fullName;
	private float age;
	public String getFullName() {
		return fullName;
	}
	public float getAge() {
		return age;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public void setAge(float age) {
		this.age = age;
	}

}
