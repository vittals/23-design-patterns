package com.designpatterns.structural.adaptorpattern;

public class ActiveBankLoan 
{
	ActiveBankApplication application;
	
	public void apply (ActiveBankApplication application)
	{
		System.out.println(application.getName() + " applied for loan at Active Bank.");
		this.application = application;
	}

}
