package com.designpatterns.structural.adaptorpattern;

public class BostonLoanApplicationAdapter 
{
	
	public BostonBankApplication converApplication(ActiveBankApplication activeApplication)
	{
		BostonBankApplication bostonApplication = new BostonBankApplication();
		bostonApplication.setFullName(activeApplication.getName());
		bostonApplication.setAge(new Float(activeApplication.getAge() / 1.0));
		return bostonApplication;
	}

}
