package com.designpatterns.structural.adaptorpattern;

public class AdapterPatternDemo 
{
	public static void main(String[] args) 
	{
		ActiveBankApplication activeApplication = new ActiveBankApplication();
		activeApplication.setName("John Smith");
		activeApplication.setAge(35);
		ActiveBankLoan activeBankLoan = new ActiveBankLoan();
		activeBankLoan.apply(activeApplication);
		
		BostonBankLoan bostonBankLoan = new BostonBankLoan();
		bostonBankLoan.apply(new BostonLoanApplicationAdapter().converApplication(activeApplication));
		
		
	}
	

}
